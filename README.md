# README # 

* Authors: Nicholas Thibodeaux and Greg Patrick
* Date: 10/10/2014


### How do I get set up? ###

* Establish destination for repo within file system - clone to SourceTree
* Begin work on Lexical Analyser


### Design Overview ###

 The design pattern was modelled on that of the specification's UML outline. 
 Development was shared (pair-programming)

##  Lexical Analyser ##
In lexical analysis, the input file is broken into individual words or symbols. These words or symbols are then represented as tokens and passed to the parser. Your lexical analyzer needs to read ASCII characters from standard input and do the following:
1. discard white space (space, tab, newline, carriage-return, and form-feed characters), 2. discard comments (everything from a semicolon to the end of the line),
3. recognize quotes, dots, and open and closing parentheses,
4. recognizethebooleanconstants#tand#f,
5. recognize integer constants (for simplicity only decimal digits without a sign), 6. recognize string constants (anything between double quotes),
7. recognize identifiers.

## Parser ##
The parser gets tokens from the scanner and analyzes the syntactic structure of the token stream. Since Scheme has a very simple grammar, parsing using a recursive-descent parser is not difficult.
For details of the syntax of Scheme and the meaning of these constructs, you can refer to the Revised5 Scheme Report - http://people.csail.mit.edu/jaffer/r5rs_9.html

## Parse Tree ##
For Scheme, the parse trees are really just regular Scheme lists. However, since we are not programming in Scheme, we need to implement the list data structure. The typical way to implement such a data structure in an object-oriented language is as a class hierarchy:
* class Node { ... };
*  class Ident : public Node { ... };
* class BoolLit : public Node { ... };
* class IntLit : public Node { ... };
* class StrLit : public Node { ... };
* class Nil : public Node { ... };
* class Cons : public Node { ... };


## Pretty Printing ##
The object-oriented style of structuring the pretty-printing code is to include a virtual method print() in each class of the parse tree node hierarchy as well as in each class of the Special hierarchy. I suggest you pass the current position on the output line as argument to print(). Also, for printing lists, you will need a boolean parameter indicating whether the opening parenthesis has been printed already or not.
For any expressions for which more than one of the above printing rules applies, the printing style is undefined. This means you can decide how to format it.

### What works and what doesn't ###

The only aspect of the assignment that we failed to get working 100% was some of the print formatting using DEFINE in conjunction with IF and LAMBDA. We have left comments in the appropriate classes that provide instruction as how you can get this semi-working for testing/grading purposes.