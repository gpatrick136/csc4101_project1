import java.io.*;

// DEBUGGING NOTE: New line problem only!
class Let extends Special {
 
	// The special forms begin, let, and cond are printed with 
	// the keyword immediately following the left parenthesis and 
	// with subsequent lines indented by four spaces each,
	// e.g.
	//	(begin
	//		(set! x 6)
	//		(set! y 7)
	//		(* x y)
	//	)
	// Sub expressions of special forms, are printed as regular lists.

 
    // Add an appropriate constructor.
	void print(Node t, int n, boolean p) {
    	if(!p) {
    		System.out.print("(");
    	}
    	System.out.println("let");
    	if (t.getCdr() != null) {
    		printElements((Cons)t.getCdr(), n, false);
    	}
    	for(int i = 0; i < n; i++) {
    		System.out.print(" ");
    	}
    	System.out.print(")");
    }
    
    void printElements(Cons t, int n, boolean isQuote) {
    	if(isQuote) {
    		System.out.print(" ");
    		t.getCar().printQuote(n, false);
    	}
    	else {
    		System.out.print("    ");
    		for(int i = 0; i < n; i++) {
    			System.out.print(" ");
    		}
    		System.out.print(" ");
    		t.getCar().print(n);
    		System.out.println();
    	}
    	
    	if(t.getCdr() != null) {
    		printElements((Cons)t.getCdr(), n, isQuote);
    	}
    }
    
    void printQuote(Node t, int n, boolean p) {
    	if(!p) {
    		System.out.print("(");
    	}
    	System.out.print("let ");
    	if(t.getCdr() != null) {
    		printElements((Cons)t.getCdr(), 0, true);
    		System.out.print(")");
    	}
    }
}