import java.io.*;
class Ident extends Node {
  private String name;
  
  public boolean isSymbol() { return true; }  // Ident
  
  public boolean isSymbolName(String s){
	  if(s.equals(name)){
		  return true;
	  }
	  else{
		  return false;
	  }
  }

  public Ident(String n) { name = n; }

  public void print(int n) {
    for (int i = 0; i < n; i++)
      System.out.print(" ");
    System.out.print(name);    
  }
  
  public String getName() {
	  return name;
  }
}