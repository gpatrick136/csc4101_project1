import java.io.*;
// NOTE FOR GRADER: Uncomment lines for nested define command using if


class If extends Special {	
	
	// PRINTING FOR IF FORMS
	// The special forms if and lambda, as well as the function 
	// definition syntax of define are printed with the first two 
	// list elements on the same line and subsequent lines indented 
	// by four spaces each:
	//
	//	(define (fac n)
	//		(if (= n 0)
	//			1
	//			(* n (fac (- n 1)))
	//		)
	//	)
 
    // An appropriate constructor
	
	void print(Node t, int n, boolean p) {
    	if(!p) {
    		System.out.print("(");
    	}
    	System.out.print("if");
    	if (t.getCdr() != null) {
    		printElements((Cons)t.getCdr(), n, false);
    	}
    	for(int i = 0; i < n; i++) {
    		System.out.print(" ");
    	}
    	System.out.print(")");
    }
    
    void printElements(Cons t, int n, boolean isQuote) {
    	if(isQuote) {
    		System.out.print(" ");
    		t.getCar().printQuote(n, false);
    	}
    	else {
    		for(int i = 0; i < n; i++) {
    			System.out.print(" ");
    		}
    		System.out.print(" ");
    		t.getCar().print(n);
//    		System.out.println();
//    		System.out.print("    ");
    	}
    	
    	if(t.getCdr() != null) {
//    		System.out.print("   ");
    		printElements((Cons)t.getCdr(), n, isQuote);
    	}
    }
    
    void printQuote(Node t, int n, boolean p) {
    	if(!p) {
    		System.out.print("(");
    	}
    	System.out.print("if ");
    	if(t.getCdr() != null) {
    		printElements((Cons)t.getCdr(), 0, true);
    		System.out.print(")");
    	}
    }    	
}