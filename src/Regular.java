import java.io.*;

class Regular extends Special {
 
    // A cons cell (an object of class Cons) will then contain an object 
	// of class Special. When constructing a cons cell, the constructor of 
	// class Cons will parse the list, build an object of the appropriate 
	// subclass of Special, and keep a pointer to that object in the cons cell.
	// Any code that is in common between all special forms can be kept in class 
	// Special. Any code for regular function applications or lists as data 
	// structures will be in class Regular.
	
	private Cons cons;

 
    // Add an appropriate constructor.
	public Regular(Cons c) {
		this.cons = c;
	}

    void print(Node t, int n, boolean p) {
    	if(!p) {
    		System.out.print("(");
    	}
    	if(cons.getCar() instanceof Cons || cons.getCar() instanceof Nil) {
    		// car of cons to return 0 and false
    		cons.getCar().print(n,false);
    	}
    	else {
    		// car of cons to return 0 and true
    		cons.getCar().print(n, true);
    	}
    	if(cons.getCdr() != null) {
    		System.out.print(" ");
    	}
    	if (cons.getCdr() != null) {
    		// cdr of cons to return 0 and true
    		cons.getCdr().print(n, true);
    	}
    	else {
    		System.out.print(")");
    	}
    }

	@Override
	void printQuote(Node t, int n, boolean p) {
		print(t, n, p);
	}
	
	
	// Return cons object
	public Cons getCons() {
		return cons;
	}
}