import java.io.*;
class IntLit extends Node {
  private int intVal;
  
  public boolean isNumber() { return true; }  // IntLit

  public IntLit(int i) { intVal = i; }

  public void print(int n) {
    for (int i = 0; i < n; i++)
      System.out.print(" ");

    System.out.print(intVal);
  }
}