import java.io.*;

// DEBUGGING NOTE: new line problem only!
class Cond extends Special {
 
    // TODO: Add any fields needed.

 
    // Add an appropriate constructor.
	public Cond() { }

    void print(Node t, int n, boolean p) {
    	if(!p) {
    		System.out.print("(");
    	}
    	System.out.println("cond");
    	if(t.getCdr() != null) {
    		printElements((Cons)t.getCdr(), n, false);
    	}
    	for(int i = 0; i < n; i++) {
    		System.out.print(" ");
    	}
    	System.out.print(")");
    }
    
    
    
    void printElements(Cons t, int n, boolean isQuote) {
    	if(isQuote) {
    		System.out.print(" ");
    		t.getCar().printQuote(n, false);
    	}
    	else {
    		System.out.print("    ");
    		for(int i = 0; i < n; i++) {
    			System.out.print(" ");
    		}
    		t.getCar().print(n);
    		System.out.println();
    	}
    	if(t.getCdr() != null) {
    		printElements((Cons)t.getCdr(), n, isQuote);
    	}
    }	
    	
    	
    	void printQuote(Node t, int n, boolean p) {
    		if(!p) {
    			System.out.print("(");
    		}
    		System.out.print("cond ");
    		if(t.getCdr() != null) {
    			printElements((Cons)t.getCdr(), 0, true);
    		}
    		System.out.print(")");
    	}
    
    
    
}