import java.io.*;
//DEBUGGING NOTE: Both versions of quote remove first bracket
//				   and do new line problem too.

class Quote extends Special {
	
	private Cons list = null;
 
    // TODO: Add an appropriate constructor.
	public Quote(Cons list) {
		this.list =list;
	}
	
    void print(Node t, int n, boolean p) {
    	System.out.print("'");
    	((Cons)list.getCar()).printQuote(n,  false);
    }
    
    void printQuote(Node t, int n, boolean p) {
    	print(t, n, p);
    }
}