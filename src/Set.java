import java.io.*;

// DEBUGGING NOTE: new line problem only!

// Cons expressions that are not special forms 
// (i.e., regular lists) as well as set! special 
// form and the variable definition syntax of define 
// are printed in the style:
// e.g.
// (+ 2 3)
// (define x 0)
// (set! x (+ 2 3))

class Set extends Special {
 
	private Cons cons;
	
	public Set(Cons c) {
		this.cons = c;
	}
	
    void print(Node t, int n, boolean p) {
    	if(!p) {
    		System.out.print("(");
    	}
    	if (cons.getCar() instanceof Cons) {
			cons.getCar().print(n, false);	
    	}
    	else { 
    		cons.getCar().print(n, false);
    	}

    	if (cons.getCdr() != null) {
			System.out.print(" ");
    	}
    	if (cons.getCdr() != null) {
    		cons.getCdr().print(n, true);
		}
		else {
			System.out.print(")");		
		}
    }
    
    
    
    
    void printQuote(Node t, int n, boolean p){
    	print(t, n, p);
    }
}