class Cons extends Node {
    private Node car;
    private Node cdr;
    private Special form;
  
    // parseList() `parses' special forms, constructs an appropriate
    // object of a subclass of Special, and stores a pointer to that
    // object in variable form.  It would be possible to fully parse
    // special forms at this point.  Since this causes complications
    // when using (incorrect) programs as data, it is easiest to let
    // parseList only look at the car for selecting the appropriate
    // object from the Special hierarchy and to leave the rest of
    // parsing up to the interpreter.
   
    void parseList() {
    	
    	if(getCar().isSymbolName("if")){
    		form = new If();
    	}
    	else if(getCar().isSymbolName("quote") || getCar().isSymbolName("'")) {
    		form = new Quote((Cons)cdr);
    	}
    	else if(getCar().isSymbolName("begin")) {
    		form = new Begin();
    	}    	
    	else if(getCar().isSymbolName("lambda")) {
    		form = new Lambda();
    	}
    	else if(getCar().isSymbolName("let")) {
    		form = new Let();
    	}
    	else if(getCar().isSymbolName("cond")) {
    		form = new Cond();
    	}
    	else if(getCar().isSymbolName("define")) {
    		form = new Define();
    	}
    	else if(getCar().isSymbolName("set!")) {
    		form = new Set(this);
    	}
    	else{
    		form = new Regular(this);
    	
    	}
    }

    public Cons(Node a, Node d) {
	car = a;
	cdr = d;
	parseList();
    }
    
    public boolean isPair()   { return true; }
    
    public Node getCar() {
        return car;
     }
      
    public Node getCdr() {
      return cdr;
    }
     
    public void setCar(Node a) {
    	car = a;
    }
      
    public void setCdr(Node d) {
    	cdr = d;
    }

    void print(int n) {
	form.print(this, n, false);
    }

    void print(int n, boolean p) {
	form.print(this, n, p);
    }

}