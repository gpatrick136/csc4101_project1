import java.io.*;

// DEBUGGING NOTE: new line problem only!
class Define extends Special {
 
    // Cons expressions that are not special forms 
	// (i.e., regular lists) as well as set! special 
	// form and the variable definition syntax of define 
	// are printed in the style:
	// e.g.
	// (+ 2 3)
	// (define x 0)
	// (set! x (+ 2 3))
	// The elements of a list are separated by a single space.

 
    // Add an appropriate constructor.
	public Define() { }


    void print(Node t, int n, boolean p) {
    	if(!p) {
    		System.out.print("(");
    	}
    	System.out.print("define ");
    	if(t.getCdr() != null) {
    		t.getCdr().getCar().print(n, false);
    	}
    	if(t.getCdr().getCdr() != null) {
    		printElements((Cons)t.getCdr().getCdr(), n, false);
    	}
    	for(int i = 0; i < n; i++) {
    		System.out.print(" ");
    	}
    	System.out.print(")");
    }
     

    
    
    void printElements(Cons t, int n, boolean isQuote) {
    	if(isQuote) {
    		System.out.print(" ");
    		t.getCar().printQuote(n, false);
    	}
    	else {
    		System.out.println();
    		System.out.print("    ");
    		for(int i = 0; i < n; i++) {
    			System.out.println();
    		}
    		t.getCar().print(n);
    		System.out.println();
    	}
    	
    	if(t.getCdr() != null) {
    		printElements((Cons)t.getCdr(), n, isQuote);
    	}
    }
    
    
    

	@Override
	void printQuote(Node t, int n, boolean p) {
		if(!p)
			System.out.print("(");
		System.out.print("define");
		if(t.getCdr() != null)
	    	printElements((Cons)t.getCdr(), 0, true);
	    	System.out.print(")");
	}
}