// In lexical analysis, the input file is broken into individual words or symbols. 
// These words or symbols are then represented as tokens and passed to the parser.
// Your lexical analyzer needs to read ASCII characters from standard input and do 
// the following:
// 1. discard white space (space, tab, newline, carriage-return, and form-feed characters),
// 2. discard comments (everything from a semicolon to the end of the line),
// 3. recognize quotes, dots, and open and closing parentheses,
// 4. recognize the boolean constants #t and #f,
// 5. recognize integer constants (for simplicity only decimal digits without a sign),
// 6. recognize string constants (anything between double quotes),
// 7. recognize identifiers.


import java.io.*;

class Scanner {
  private PushbackInputStream in;
  private byte[] buf = new byte[1000];
  private Token pushback = null; // lookahead
  StringBuffer sb;

  public Scanner(InputStream i) { in = new PushbackInputStream(i); }
    
  // Helper method to aid with lookahead using getNextToken()
  public void pushTokenBack(Token t) {
	  pushback = t;
  }
  
  // Getting next token
  public Token getNextToken() {
	  if (pushback != null) { // lookahead
		  Token temp = pushback;
		  pushback = null;
		  return temp;
	  }
    int bite = -1;
	
    // It would be more efficient if we'd maintain our own input buffer
    // and read characters out of that buffer, but reading individual
    // characters from the input stream is easier.
    try {
      bite = in.read();
    } catch (IOException e) {
      System.err.println("We fail: " + e.getMessage());
    }

    // Skip white space and comments
    char wcbite = (char)bite; // wcbite refers to white space and comments
    if (whiteSpace(wcbite)) { // Call whiteSpace
    	return getNextToken();
    }
    
    if(wcbite == ';') {
    	try {
    		while (in.read() != '\n');
    	} catch (IOException e) {
    		System.err.println("We fail: " + e.getMessage());
    		bite = -1;
    	}
    	return getNextToken();
    }
	
    if (bite == -1)
      return null;

    char ch = (char) bite;
	
    // Special characters - quotes, dots and open/closed parenthesis
    if (ch == '\'')
      return new Token(Token.QUOTE);
    else if (ch == '(')
      return new Token(Token.LPAREN);
    else if (ch == ')')
      return new Token(Token.RPAREN);
    else if (ch == '.')
      // We ignore the special identifier `...'.
      return new Token(Token.DOT);

    // Boolean constants (#t and #f)
    else if (ch == '#') {
      try {
	bite = in.read();
      } catch (IOException e) {
	System.err.println("We fail: " + e.getMessage());
      }

      if (bite == -1) {
	System.err.println("Unexpected EOF following #");
	return null;
      }
      ch = (char) bite;
      if (ch == 't')
	return new Token(Token.TRUE);
      else if (ch == 'f')
	return new Token(Token.FALSE);
      else {
	System.err.println("Illegal character '" + (char) ch + "' following #");
	return getNextToken();
      }
    }

    // String constants (anything between double quotes!)
    else if (ch == '"') {
      int i;
      for (i=0; i < buf.length; i++) {
    	  try {
    		  ch = (char)in.read();
    		  if (ch == '\\') {
    			  buf[i] = (byte)'\\';
    			  i++;
    			  ch = (char)in.read();
    			  buf[i] = (byte)ch;
    			  continue;
    		  }
    		  if (ch == '\"') {
    			  break;
    		  }
    		  buf[i] = (byte)ch;
    	  }
    	  catch(IOException e) {
    		  System.err.println("We fail: " + e.getMessage());
    	  }
    	  catch(IllegalArgumentException e) {
    		  System.err.println("Invalid, something is not right: " + e.getMessage());
    	  }
      }
      byte[] str = new byte[i];
      for(int j = 0; j < i; j++) {
    	  str[j] = buf[j];
      }
      return new StrToken(buf.toString());
    }
    

    // Integer constants (only decimal digits without a sign needed)
    else if (ch >= '0' && ch <= '9') {
      int i = ch - '0';
      while(ch >= '0' && ch <= '9'){
    	  try {
    	      bite = in.read();
    	    } catch (IOException e) {
    	      System.err.println("We fail: " + e.getMessage());
    	    }    		
    	    if (bite == -1)
    	      return null;

    	    ch = (char) bite;
    	    if(ch >= '0' && ch <= '9'){
    	    	i = 10 * i + ch - '0';
    	    }
    	    else{
    	    	try{
    	    	     in.unread(ch);
    	    	   }catch (IOException e) {
    	    	     System.err.println("We fail: " + e.getMessage());
    	    	   }     	    
    	    }
      }
  	  return new IntToken(i);
    }

    // Identifiers
    else if ((ch >= 'A' && ch <= 'Z')||(ch >= 'a' && ch <= 'z')||(ch == '!')||(ch == '$')||(ch == '%')||(ch == '&')||(ch == '*')
    		||(ch == '/')||(ch == ':')||(ch == '<')||(ch == '=')||(ch == '>')||(ch == '?')||(ch == '^')||(ch == '_')||(ch == '~')
    		||(ch >= '0' && ch <= '9')||(ch == '+')||(ch == '-')||(ch == '.')||(ch == '@')) {
    	sb = new StringBuffer();
    	sb.append(ch);
    	while((ch >= 'A' && ch <= 'Z')||(ch >= 'a' && ch <= 'z')||(ch == '!')||(ch == '$')||(ch == '%')||(ch == '&')||(ch == '*')
        		||(ch == '/')||(ch == ':')||(ch == '<')||(ch == '=')||(ch == '>')||(ch == '?')||(ch == '^')||(ch == '_')||(ch == '~')
        		||(ch >= '0' && ch <= '9')||(ch == '+')||(ch == '-')||(ch == '.')||(ch == '@')){
    		try {
      	      bite = in.read();
      	    } catch (IOException e) {
      	      System.err.println("We fail: " + e.getMessage());
      	    }    		
      	    if (bite == -1)
      	      return null;

      	    ch = (char) bite;
      	    if((ch >= 'A' && ch <= 'Z')||(ch >= 'a' && ch <= 'z')||(ch == '!')||(ch == '$')||(ch == '%')||(ch == '&')||(ch == '*')
        		||(ch == '/')||(ch == ':')||(ch == '<')||(ch == '=')||(ch == '>')||(ch == '?')||(ch == '^')||(ch == '_')||(ch == '~')
        		||(ch >= '0' && ch <= '9')||(ch == '+')||(ch == '-')||(ch == '.')||(ch == '@')){
      	    	sb.append(ch);
      	    }
      	    else{
      	    	try{
   	    	     in.unread(ch);
   	    	   }catch (IOException e) {
   	    	     System.err.println("We fail: " + e.getMessage());
   	    	   } 
      	    }
    		
    	}
      String identString = new String(sb);
      return new IdentToken(identString);
    }

    // Illegal character
    else {
      System.err.println("Illegal input character '" + (char) ch + '\'');
      return getNextToken();
    }
  };
  
  // Establishing the white space characters to be used in getNextToken
  private boolean whiteSpace(char ch) {
	  return ch == ' ' || ch == '\t' || ch == '\n' || ch == '\r' || ch == '\f';
  }
  
}