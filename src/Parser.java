// Parser.java -- the implementation of class Parser
//
// Defines
//
//   class Parser;
//
// Parses the language
//
//   exp  ->  ( rest
//         |  #f
//         |  #t
//         |  ' exp
//         |  integer_constant
//         |  string_constant
//         |  identifier
//    rest -> )
//         |  exp+ [. exp] )
//
// and builds a parse tree.  Lists of the form (rest) are further
// `parsed' into regular lists and special forms in the constructor
// for the parse tree node class Cons.  See Cons.parseList() for
// more information.
//
// The parser is implemented as an LL(0) recursive descent parser.
// I.e., parseExp() expects that the first token of an exp has not
// been read yet.  If parseRest() reads the first token of an exp
// before calling parseExp(), that token must be put back so that
// it can be reread by parseExp() or an alternative version of
// parseExp() must be called.
//
// If EOF is reached (i.e., if the scanner returns a NULL) token,
// the parser returns a NULL tree.  In case of a parse error, the
// parser discards the offending token (which probably was a DOT
// or an RPAREN) and attempts to continue parsing with the next token.

class Parser {
  
  // Scanner object
  private Scanner scanner;
  
  // Boolean literal expressions
  private BooleanLit trueLit = new BooleanLit(true);
  private BooleanLit falseLit = new BooleanLit(false);
  
  // Nil object
  private Nil n = new Nil();
  

  public Parser(Scanner s) { scanner = s; }
  
  public Node parseExp() {
    // TODO: write code for parsing an exp (Ref Notes)
	  Node exp = null;
	  Token t = scanner.getNextToken();
	  
	  if (t == null) {	//EOF
		  exp = null;
	  } else if (t.getType() == TokenType.LPAREN) {	//left parenthesis
		  exp = parseRest(true);
	  } else if (t.getType() == TokenType.FALSE) {	//boolean false
	  	  exp = falseLit;
  	  } else if (t.getType() == TokenType.TRUE) {	//boolean true	
  		  exp = trueLit;
  	  } else if (t.getType() == TokenType.QUOTE) {	//inverted commas
  		  exp = new Cons(new Ident("'"), new Cons(parseExp(), null));
  	  } else if (t.getType() == TokenType.INT) {	//integers
  		  exp = new IntLit(t.getIntVal());
  	  } else if (t.getType() == TokenType.STRING) {	//strings
  		  exp = new StrLit(t.getStrVal());
  	  } else if (t.getType() == TokenType.IDENT) {	//identifier
  		  exp = new Ident(t.getName());
  	  } else if (t.getType() == TokenType.RPAREN) {	//right parenthesis
  		  System.out.println("Unexpected Token: )");
  		  exp = parseExp();
  	  } else { //Should there be an issue with parsing...
  		  System.out.println(t.getType() + " is not a valid Token Type");
  	  }
	 
    return exp;
  }
  
//   TODO: write code for parsing rest
//	 TODO: Investigate how we can develop out Cons nodes
//	 We want to recursively call parseExp() and ParseRest()
//	 Taking the parse trees produced and combining
//	 e.g.   a = parseExp()
//	 		d = parseRest()
//	 return new Cons(a,d) (we want to return exp!) 
 
protected Node parseRest(boolean first){
	  Token tok = scanner.getNextToken();
	  Node exp = null;
	  if(tok == null) {
			exp = null;
	  } else if(tok.getType() == TokenType.RPAREN) {
		  if (first) return new Nil();
		  else return null;
	  } else if(tok.getType() == TokenType.DOT) {
		  // Perform lookahead at this point!
		  tok = scanner.getNextToken();
		  if(tok.getType() != TokenType.RPAREN) {
			  // Use pushBackToken here!!!
			  
			   scanner.pushTokenBack(tok);
			   exp = new Cons(parseExp(), null);
			  
			  // ELSE: Show error message because parnthesis isn't recognized
			  // exp = parseExp()
		  } else {
			  System.out.println("')' not recognized for tokenising");
			  exp = parseExp();
		  }    
			  // ELSE: scanner.pushTokenBack(tok);
			  // exp = new Cons(parseExp(), parseRest(false));
	  } else { 
		  scanner.pushTokenBack(tok);
		  exp = new Cons(parseExp(), parseRest(false));
		  }
		  
	  // Return parsed node
	  return exp;
	}
 
};